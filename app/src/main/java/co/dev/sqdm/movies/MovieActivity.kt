package co.dev.sqdm.movies

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import co.dev.sqdm.movies.Adapter.MovieAdapter
import co.dev.sqdm.movies.DataBase.Database
import co.dev.sqdm.movies.Service.MovieService
import co.dev.sqdm.movies.Service.ServiceFactory
import co.dev.sqdm.movies.Utils.Constants
import co.dev.sqdm.movies.Model.Movie
import com.squareup.picasso.Target
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MovieActivity : AppCompatActivity() {
    lateinit var a:Target
    var id: Int =0
    lateinit var movieAdapter: MovieAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_movie)
        if (savedInstanceState == null) {
            val extras = intent.extras
            if (extras == null) {
                id = 0
            } else {
                id = extras.getInt(resources.getString(R.string.id))
            }
        } else {
            id = savedInstanceState.getSerializable(resources.getString(R.string.id)) as Int
        }
        val mRecyclerView = findViewById(R.id.recycler_view) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.setLayoutManager(LinearLayoutManager(this))
        movieAdapter = MovieAdapter()
        mRecyclerView.setAdapter(movieAdapter)

        val service = ServiceFactory.createRetrofitService(MovieService::class.java, MovieService.SERVICE_ENDPOINT)
        //                for(String login : Data.githubList) {
        service.getMovie(Constants.API_KEY, resources.getString(R.string.json), "id:${id.toString()}")
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> movieAdapter.addData(result) },
                { error -> setMovies_offline() }
            )
    }
    private fun setMovies_offline(){
        val manager = Database(applicationContext, null, null, 1)
        var movie: Movie?=null
        try {
            movie = manager.readMovie(id.toString())
        } catch (e: Exception) {
            e.printStackTrace()
        }
        if (movie != null) {
            movieAdapter.addData(movie)
        }

    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.settings->{
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}