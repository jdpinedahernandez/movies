package co.dev.sqdm.movies.Adapter

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import co.dev.sqdm.movies.MovieActivity
import co.dev.sqdm.movies.R
import co.dev.sqdm.movies.Model.Movie
import co.dev.sqdm.movies.Model.MovieDetail
import com.squareup.picasso.Picasso
import java.util.ArrayList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Base64
import co.dev.sqdm.movies.DataBase.Database
import java.io.ByteArrayOutputStream
import android.graphics.drawable.Drawable
import android.text.Html
import android.util.Log
import java.lang.Exception


class MoviesAdapter : RecyclerView.Adapter<MoviesAdapter.ViewHolder>() {
    internal var mItems: MutableList<MovieDetail>
    lateinit var context:Context
    init {
        mItems = ArrayList<MovieDetail>()
    }

    fun addData(movie: Movie) {
        mItems=(movie.results)
        notifyDataSetChanged()
    }

    fun clear() {
        mItems.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_movie, viewGroup, false)
        context = viewGroup.context
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {

        val movieDetail = mItems[i]
        if (movieDetail.name!=null)
            viewHolder.title.text = movieDetail.name
        if (movieDetail.deck!=null)
            viewHolder.description.text = Html.fromHtml(movieDetail.deck)

        var imageDrawable:Drawable
        if (movieDetail.imageDrawable!=null)
            imageDrawable = movieDetail.imageDrawable
        else
            imageDrawable = ColorDrawable(Color.RED)
            Picasso.get()
                .load(movieDetail.image?.small_url)
                .placeholder(imageDrawable)
                .error(imageDrawable)
                .into(object: com.squareup.picasso.Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        Log.i("","")
                        viewHolder.grid_item_image.setImageDrawable(placeHolderDrawable)
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        Log.i("","")
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        viewHolder.grid_item_image.setImageBitmap(bitmap)
                        encodeSave(bitmap,movieDetail)
                    }

                })
//        }
        viewHolder.panel.setOnClickListener {
            val intent = Intent(context, MovieActivity::class.java)
            intent.putExtra(context.resources.getString(R.string.id),movieDetail.id)
            context.startActivity(intent)
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var description: TextView
        var grid_item_image: ImageView
        var panel: LinearLayout

        init {
            title = itemView.findViewById(R.id.title) as TextView
            description = itemView.findViewById(R.id.description) as TextView
            grid_item_image = itemView.findViewById(R.id.grid_item_image) as ImageView
            panel = itemView.findViewById(R.id.panel) as LinearLayout
        }

    }

    private fun encodeSave(bitmap: Bitmap?, movieDetail: MovieDetail){
        val baos = ByteArrayOutputStream()
        bitmap?.compress(Bitmap.CompressFormat.JPEG, 30, baos)
        val b = baos.toByteArray()
        val encImage = Base64.encodeToString(b, Base64.DEFAULT)
        movieDetail.image_base64=encImage
        val manager = Database(context, null, null, 1)
        manager.updateMovie(movieDetail)
    }


}