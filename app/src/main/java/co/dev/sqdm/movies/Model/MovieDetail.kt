package co.dev.sqdm.movies.Model

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable

class MovieDetail {
    var deck: String? = ""
    var name: String? = ""
    var budget: String? = ""
    var box_office_revenue: String? = ""
    var release_date: String? = ""
    var id: Int = 0
    var description: String? = ""
    var image: MovieDetailImages?=null
    var image_base64: String? = ""
    @Transient
    var imageDrawable:Drawable = ColorDrawable(Color.RED)
    constructor(id: Int, name: String?, budget: String?,box_office_revenue:String?,
                release_date:String?,description:String?,deck:String?,image:String?,imageDrawable: Drawable) {
        this.id = id
        this.name = name
        this.deck = deck
        this.budget = budget
        this.box_office_revenue = box_office_revenue
        this.release_date = release_date
        this.description = description
        this.image_base64 = image
        this.imageDrawable = imageDrawable
    }
}
