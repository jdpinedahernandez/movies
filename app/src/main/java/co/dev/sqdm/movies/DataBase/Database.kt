package co.dev.sqdm.movies.DataBase

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.graphics.BitmapFactory
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.util.Base64
import co.dev.sqdm.movies.Model.*
import java.lang.Exception
import java.util.ArrayList

class Database(context: Context, nombre: String?,
                      factory: SQLiteDatabase.CursorFactory?, version: Int) :

    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    val context:Context = context
    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "MoviesDB"
        val TABLE_USER = "User"
        val TABLE_MOVIE = "Movie"

        val ID_COLUMN = "id"
        val NAME_COLUMN = "name"
        val EMAIL_COLUMN = "email"
        val IMAGE_COLUMN = "image"
        val AGE_COLUMN = "age"
        val DECK_COLUMN = "deck"
        val BUDGET_COLUMN = "budget"
        val REVENUE_COLUMN = "box_office_revenue"
        val DATE_COLUMN = "release_date"
        val DESCRIPTION_COLUMN = "description"
    }

    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_DATA_BASE_USER = ("CREATE TABLE $TABLE_USER($ID_COLUMN INTEGER PRIMARY KEY, $NAME_COLUMN TEXT, $EMAIL_COLUMN TEXT,$IMAGE_COLUMN TEXT,$AGE_COLUMN TEXT) ")
        val CREATE_DATA_BASE_MOVIE = ("CREATE TABLE $TABLE_MOVIE($ID_COLUMN INTEGER PRIMARY KEY, $NAME_COLUMN TEXT, $IMAGE_COLUMN TEXT,$DECK_COLUMN TEXT,$BUDGET_COLUMN TEXT,$REVENUE_COLUMN TEXT,$DATE_COLUMN TEXT,$DESCRIPTION_COLUMN TEXT) ")

        db!!.execSQL(CREATE_DATA_BASE_USER)
        db!!.execSQL(CREATE_DATA_BASE_MOVIE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        if (newVersion > oldVersion) {
            db!!.execSQL("DROP TABLE IF EXISTS $TABLE_USER")
            db!!.execSQL("DROP TABLE IF EXISTS $TABLE_MOVIE")
            onCreate(db)
        }
    }

    fun insertUser(userDTO: User) {
        val valor = ContentValues()
//        valor.put(ID_COLUMN, userDTO.id)
        valor.put(NAME_COLUMN, userDTO.name)
        valor.put(EMAIL_COLUMN, userDTO.email)
        valor.put(IMAGE_COLUMN, userDTO.image)
        valor.put(AGE_COLUMN, userDTO.age)

        val db = this.writableDatabase

        db.insert(TABLE_USER, null, valor)
        db.close()
    }

    fun readUser(email: String): User? {
        val query = "SELECT * FROM $TABLE_USER WHERE $EMAIL_COLUMN = \"$email\""
        val db = this.readableDatabase

        val cursor = db.rawQuery(query, null)
        var user: User? = null

        if (cursor.moveToFirst()) {

            val id = Integer.parseInt(cursor.getString(0))
            val name = cursor.getString(1)
            val email = cursor.getString(2)
            val image = cursor.getString(3)
            val age = cursor.getString(4)

            user = User(id, name, email, image, age)

            cursor.close()
        }

        db.close()
        return user
    }
    fun updateUser(userDTO: User) {
        val whereCls = "$EMAIL_COLUMN = '" + userDTO.email + "'"
        val valor = ContentValues()
        valor.put(ID_COLUMN, userDTO.id)
        valor.put(NAME_COLUMN, userDTO.name)
        valor.put(EMAIL_COLUMN, userDTO.email)
        valor.put(IMAGE_COLUMN, userDTO.image)
        valor.put(AGE_COLUMN, userDTO.age)
        val db = this.writableDatabase
        db.update(TABLE_USER,valor, whereCls, null)
        db.close()
    }

    fun deleteUser(name: String): Boolean {
        var resultado = false

        val query = "SELECT * FROM $TABLE_USER WHERE $NAME_COLUMN = \"$name\""
        val db = this.writableDatabase

        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            val id = Integer.parseInt(cursor.getString(0))
            db.delete(TABLE_USER, "$ID_COLUMN = ?", arrayOf(id.toString()))
            cursor.close()

            resultado = true
        }

        db.close()
        return resultado
    }
    fun insertMovie(movie: MovieDetail) {
        val valor = ContentValues()
        valor.put(ID_COLUMN, movie.id)
        valor.put(NAME_COLUMN, movie.name)
        valor.put(IMAGE_COLUMN, movie.image_base64)
        valor.put(DECK_COLUMN, movie.deck)
        valor.put(BUDGET_COLUMN, movie.budget)
        valor.put(REVENUE_COLUMN, movie.box_office_revenue)
        valor.put(DATE_COLUMN, movie.release_date)
        valor.put(DESCRIPTION_COLUMN, movie.description)
        val db = this.writableDatabase
        db.insert(TABLE_MOVIE, null, valor)
        db.close()
    }
    fun readMovies(): Movie? {
        var movieDetailList:ArrayList<MovieDetail> =ArrayList()
        var movie: Movie =Movie()
        val query = "SELECT * FROM $TABLE_MOVIE"
        val db = this.readableDatabase

        val cursor = db.rawQuery(query, null)
        var movieDetail: MovieDetail

        if (cursor.moveToFirst()) {

            cursor.moveToFirst()
            do {
            val id = Integer.parseInt(cursor.getString(0))
            val name = cursor.getString(1)
            val image = cursor.getString(2)
            val deck = cursor.getString(3)
            val budget = cursor.getString(4)
            val revenue = cursor.getString(5)
            val date = cursor.getString(6)
            val description = cursor.getString(7)
                var drawable=decodeBase64(image)

                movieDetail = MovieDetail(id, name, budget, revenue, date, description, deck,image,drawable)
                movieDetailList.add(movieDetail)
            } while (cursor.moveToNext())
            movie.results=movieDetailList

        }

        db.close()
        return movie
    }
    fun readMovie(id: String): Movie? {
        val query = "SELECT * FROM $TABLE_MOVIE WHERE $ID_COLUMN = \"$id\""
        val db = this.readableDatabase
        var movie:Movie=Movie()
        var movieDetailList:ArrayList<MovieDetail> =ArrayList()

        val cursor = db.rawQuery(query, null)
        var movieDetail: MovieDetail? = null

        if (cursor.moveToFirst()) {
            cursor.moveToFirst()

            val id = Integer.parseInt(cursor.getString(0))
            val name = cursor.getString(1)
            val image = cursor.getString(2)
            val deck = cursor.getString(3)
            val budget = cursor.getString(4)
            val revenue = cursor.getString(5)
            val date = cursor.getString(6)
            val description = cursor.getString(7)
            var drawable=decodeBase64(image)

            movieDetail = MovieDetail(id, name, budget, revenue, date, description, deck,image,drawable)
            movieDetailList.add(movieDetail)
            movie.results=movieDetailList
            cursor.close()
        }

        db.close()
        return movie
    }
    fun updateMovie(movie: MovieDetail) {
        val whereCls = "$ID_COLUMN = '" + movie.id+ "'"
        val valor = ContentValues()
        valor.put(NAME_COLUMN, movie.name)
        valor.put(IMAGE_COLUMN, movie.image_base64)
        valor.put(DECK_COLUMN, movie.deck)
        valor.put(BUDGET_COLUMN, movie.budget)
        valor.put(REVENUE_COLUMN, movie.box_office_revenue)
        valor.put(DATE_COLUMN, movie.release_date)
        valor.put(DESCRIPTION_COLUMN, movie.description)
        val db = this.writableDatabase
        db.update(TABLE_MOVIE,valor, whereCls, null)
        db.close()
    }
    fun decodeBase64(input: String?): Drawable {
        var drawable: BitmapDrawable = BitmapDrawable()
        try {
            val decodedByte = Base64.decode(input, 0)
            val bitmap = BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
            drawable = BitmapDrawable(context.getResources(), bitmap)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return drawable
    }
}