package co.dev.sqdm.movies.Service;

import co.dev.sqdm.movies.Model.Movie;
import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

public interface MovieService {
    String SERVICE_ENDPOINT = "https://comicvine.gamespot.com/api";

    @GET("/movies/")
    Observable<Movie> getMovie(@Query("api_key") String api,
                               @Query("format") String format,
                               @Query("filter") String id);
    @GET("/movies/")
    Observable<Movie> getMovies(@Query("api_key") String api,
                               @Query("format") String format);
}
