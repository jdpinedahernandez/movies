package co.dev.sqdm.movies

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import co.dev.sqdm.movies.DataBase.Database
import co.dev.sqdm.movies.Model.User
import android.content.Intent
import android.graphics.BitmapFactory
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.content.pm.PackageManager
import android.Manifest.permission
import android.content.SharedPreferences
import android.widget.*
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.util.Base64
import co.dev.sqdm.movies.Utils.Constants
import java.io.ByteArrayOutputStream
import android.widget.ArrayAdapter
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions


class SettingsActivity : AppCompatActivity() {
    private lateinit var actualizar : Button
    private lateinit var borrar: Button
    private lateinit var cerrar: Button
    private lateinit var name: EditText
    private lateinit var email: EditText
    private lateinit var image: ImageView
    private lateinit var age: Spinner

    private val TAG = "SettingsActivity"
    private var picturePath = ""
    var prefs: SharedPreferences? = null
    private val RESULT_LOAD_IMAGE = 1
    private val REQUEST_EXTERNAL_STORAGE = 2
    private val PERMISSIONS_STORAGE =
        arrayOf<String>(permission.READ_EXTERNAL_STORAGE, permission.WRITE_EXTERNAL_STORAGE)
    private lateinit var mGoogleSignInClient : GoogleSignInClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.settings)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        prefs = this.getSharedPreferences(Constants.PREF_NAME, 0)
        actualizar = findViewById(R.id.btn_actualizar) as Button
        borrar = findViewById(R.id.continuar_btn) as Button
        cerrar = findViewById(R.id.btn_cerrar_sesion) as Button
        image = findViewById(R.id.image_view) as ImageView
        name = findViewById(R.id.nombre_edt) as EditText
        email = findViewById(R.id.email_edt) as EditText
        age = findViewById(R.id.age_spn) as Spinner
        var ages = ArrayList<String>()
        for (value in 1..150){
            ages.add(value.toString())
        }
        val adapter_age = ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_item, ages
        )
        adapter_age.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        age.setAdapter(adapter_age)
        consultar(prefs!!.getString(Constants.EMAIL,""))

        actualizar.setOnClickListener {
            val name = name.text.toString()
            val email = email.text.toString()
            val ages = age.selectedItem.toString()
            val baos = ByteArrayOutputStream()
            val bitmap = (image.getDrawable() as BitmapDrawable).bitmap
            bitmap.compress(Bitmap.CompressFormat.JPEG, 30, baos)
            var imageBytes = baos.toByteArray()
            val imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT)
            var a = imageString.length
            val manager = Database(applicationContext, null, null, 1)
            val user = User(name, email, imageString, ages)
            manager.updateUser(user)
            finish()
        }

        image.setOnClickListener {
            verifyStoragePermissions(this@SettingsActivity)

        }
        cerrar.setOnClickListener {
            mGoogleSignInClient.signOut()
                .addOnCompleteListener(this) {
                    Toast.makeText(this,resources.getString(R.string.close),Toast.LENGTH_LONG).show()
                    val intent = Intent(this, MainActivity::class.java)
                    startActivity(intent)
                    finish()
                }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == RESULT_LOAD_IMAGE && resultCode == Activity.RESULT_OK && null != data) {
            val selectedImage = data.data
            val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

            val cursor = contentResolver.query(
                selectedImage!!,
                filePathColumn, null, null, null
            )
            cursor!!.moveToFirst()

            val columnIndex = cursor.getColumnIndex(filePathColumn[0])
            picturePath = cursor.getString(columnIndex)
            cursor.close()

            image.setImageBitmap(BitmapFactory.decodeFile(picturePath))
        }
    }

    fun verifyStoragePermissions(activity: Activity) {
        // Check if we have write permission
        val permission = ActivityCompat.checkSelfPermission(activity, permission.READ_EXTERNAL_STORAGE)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_STORAGE,
                REQUEST_EXTERNAL_STORAGE
            )
        }
        else{
            val i = Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(i, RESULT_LOAD_IMAGE)
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        when (requestCode) {
            REQUEST_EXTERNAL_STORAGE -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    val i = Intent(
                        Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    )

                    startActivityForResult(i, RESULT_LOAD_IMAGE)
                } else {
                    Toast.makeText(applicationContext, resources.getString(R.string.no_permission), Toast.LENGTH_SHORT).show()
                }
                return
            }
        }// other 'case' lines to check for other
        // permissions this app might request
    }
    private fun consultar(email_string: String){
        val manager = Database(applicationContext, null, null, 1)
        val user = manager.readUser(email_string)

        if (user != null) {
            name.setText(user.name.toString())
            email.setText(user.email.toString())
            val myString = "some value" //the value you want the position for
            age.setSelection((age.getAdapter() as ArrayAdapter<String>).getPosition(user.age))
            //decode base64 string to image
            var imageBytes = Base64.decode(user.image.toString(), Base64.DEFAULT)
            val decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.size)
            if (user.image.toString().equals("",true))
                image.setImageResource(R.mipmap.ic_launcher_round)
            else
                image.setImageBitmap(decodedImage)
        } else {
            Toast.makeText(applicationContext, resources.getString(R.string.no_user), Toast.LENGTH_SHORT).show()
        }
    }

}
