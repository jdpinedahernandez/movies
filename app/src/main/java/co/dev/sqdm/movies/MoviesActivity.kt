package co.dev.sqdm.movies

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import co.dev.sqdm.movies.Adapter.MoviesAdapter
import co.dev.sqdm.movies.DataBase.Database
import co.dev.sqdm.movies.Service.MovieService
import co.dev.sqdm.movies.Service.ServiceFactory
import co.dev.sqdm.movies.Utils.Constants
import co.dev.sqdm.movies.Model.Movie
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class MoviesActivity : AppCompatActivity() {
    lateinit var movieAdapter:MoviesAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_movie)

        val mRecyclerView = findViewById(R.id.recycler_view) as RecyclerView
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.setLayoutManager(LinearLayoutManager(this))
        movieAdapter = MoviesAdapter()
        mRecyclerView.setAdapter(movieAdapter)

        val service = ServiceFactory.createRetrofitService(MovieService::class.java, MovieService.SERVICE_ENDPOINT)
        //                for(String login : Data.githubList) {
        service.getMovies(Constants.API_KEY, resources.getString(R.string.json))
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result -> setMovies(result) },
                { error -> setMovies_offline(error) }
            )
    }
    private fun setMovies(movie: Movie){
        movieAdapter.addData(movie)
        insertMovies(movie)
    }
    private fun setMovies_offline(m:Throwable){
        val manager = Database(applicationContext, null, null, 1)
        var movie: Movie?=null
        try {
            movie = manager.readMovies()

        if (movie != null) {
            movieAdapter.addData(movie)
        }
        } catch (e: Exception) {
            Toast.makeText(this,resources.getString(R.string.no_wifi), Toast.LENGTH_LONG).show()
            finish()
            e.printStackTrace()
        }
    }
    private fun insertMovies(movie: Movie){
        val manager = Database(applicationContext, null, null, 1)
        for (i in 0 until movie.results.size){
            manager.insertMovie(movie.results[i])
        }

    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.settings->{
                val intent = Intent(this, SettingsActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }
}