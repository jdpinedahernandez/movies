package co.dev.sqdm.movies.Adapter

import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import co.dev.sqdm.movies.R
import co.dev.sqdm.movies.Model.Movie
import co.dev.sqdm.movies.Model.MovieDetail
import com.squareup.picasso.Picasso
import java.lang.Exception
import java.util.ArrayList

class MovieAdapter : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    internal var mItems: MutableList<Movie>

    init {
        mItems = ArrayList<Movie>()
    }

    fun addData(movie: Movie) {
        mItems.add(movie)
        notifyDataSetChanged()
    }

    fun clear() {
        mItems.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): ViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.adapter_movie, viewGroup, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, i: Int) {
        val movie = mItems[i]
        val movieDetail: MutableList<MovieDetail>? = movie.results
        viewHolder.title.text = movieDetail?.get(0)?.name
        try {
            viewHolder.description.text = Html.fromHtml( movieDetail?.get(0)?.description)
        } catch (e: Exception) {
        }

        if (movieDetail != null) {
            var imageDrawable:Drawable
            if (movieDetail.get(0)?.imageDrawable!=null)
                imageDrawable = movieDetail.get(0)?.imageDrawable
            else
                imageDrawable = ColorDrawable(Color.TRANSPARENT)
            Picasso.get()
                .load(movieDetail?.get(0)?.image?.small_url)
                .placeholder(imageDrawable)
                .error(imageDrawable)
                .noFade()
                .into(object: com.squareup.picasso.Target {
                    override fun onPrepareLoad(placeHolderDrawable: Drawable?) {
                        viewHolder.grid_item_image.setImageDrawable(placeHolderDrawable)
                    }

                    override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                        Log.i("","")
                    }

                    override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                        viewHolder.grid_item_image.setImageBitmap(bitmap)
                    }

                })
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var title: TextView
        var description: TextView
        var grid_item_image: ImageView

        init {
            title = itemView.findViewById(R.id.title) as TextView
            description = itemView.findViewById(R.id.description) as TextView
            grid_item_image = itemView.findViewById(R.id.grid_item_image) as ImageView
        }
    }
}