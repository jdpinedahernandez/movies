package co.dev.sqdm.movies.Model

class User {

    var id: Int = 0
    var name: String? = null
    var email: String? = null
    var image: String? = null
    var age: String? = null

    constructor(id: Int, name: String, email: String,image:String,age:String) {
        this.id = id
        this.name = name
        this.email = email
        this.image = image
        this.age = age
    }

    constructor(name: String, email: String,image:String,age:String) {
        this.name = name
        this.email = email
        this.image = image
        this.age = age
    }

}