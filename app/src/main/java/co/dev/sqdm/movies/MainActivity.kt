package co.dev.sqdm.movies

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.SignInButton
import android.content.Intent
import android.content.SharedPreferences
import android.util.Log
import android.widget.Toast
import co.dev.sqdm.movies.DataBase.Database
import co.dev.sqdm.movies.Model.User
import co.dev.sqdm.movies.Utils.Constants.Companion.EMAIL
import co.dev.sqdm.movies.Utils.Constants.Companion.PREF_NAME
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.tasks.Task
import com.google.android.gms.common.api.ApiException


class MainActivity : AppCompatActivity() {
    private lateinit var signInButton: SignInButton
    private lateinit var mGoogleSignInClient :GoogleSignInClient
    var prefs: SharedPreferences? = null
    private var TAG = "MainActitvity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        prefs = this.getSharedPreferences(PREF_NAME, 0)
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        signInButton = findViewById(R.id.sign_in_button)
        signInButton.setSize(SignInButton.SIZE_STANDARD)
        signInButton.setOnClickListener { signIn() }
    }

    override fun onStart() {
        super.onStart()
        val account = GoogleSignIn.getLastSignedInAccount(this)
        if (account!=null){
            val editor = prefs!!.edit()
            editor.putString(EMAIL, account.email!!)
            editor.apply()
            val intent = Intent(this, MoviesActivity::class.java)
            startActivity(intent)
        }
//        updateUI(account)
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, 1)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == 1) {
            // The Task returned from this call is always completed, no need to attach
            // a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {
        try {
            val account = completedTask.getResult(ApiException::class.java)

            // Signed in successfully, show authenticated UI.
            Toast.makeText(this,resources.getString(R.string.ok),Toast.LENGTH_LONG).show()
            val manager = Database(applicationContext, null, null, 1)
            val user = manager.readUser(account.email!!)
            if (user != null) {

            } else {
                val user = User(account.displayName!!, account.email!!, "", "")
                manager.insertUser(user)
            }

            val editor = prefs!!.edit()
            editor.putString(EMAIL, account.email!!)
            editor.apply()
            val intent = Intent(this, MoviesActivity::class.java)
            startActivity(intent)
//            updateUI(account)
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.statusCode)
            Toast.makeText(this,resources.getString(R.string.fallo),Toast.LENGTH_LONG).show()

//            updateUI(null)
        }

    }
}
